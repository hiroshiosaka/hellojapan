﻿namespace HelloJapan
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.HelloLabel = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.HelloLabel2 = new System.Windows.Forms.Label();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(85, 51);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(420, 368);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(820, 204);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 29);
            this.button1.TabIndex = 4;
            this.button1.Text = "北海道";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.HdButtonClicked);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(861, 319);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 24);
            this.button2.TabIndex = 5;
            this.button2.Text = "宮城";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.MgButtonClicked);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(803, 246);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(52, 29);
            this.button3.TabIndex = 6;
            this.button3.Text = "青森";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.AmButtonClicked);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(788, 282);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(64, 29);
            this.button4.TabIndex = 7;
            this.button4.Text = "秋田";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.AtButtonClicked);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(868, 268);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(55, 29);
            this.button5.TabIndex = 8;
            this.button5.Text = "岩手";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.ItButtonClicked);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(756, 319);
            this.button6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(71, 24);
            this.button6.TabIndex = 9;
            this.button6.Text = "山形";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.YgButtonClicked);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(673, 375);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(67, 29);
            this.button7.TabIndex = 10;
            this.button7.Text = "新潟";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.NgButtonClicked);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(860, 375);
            this.button8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(63, 29);
            this.button8.TabIndex = 11;
            this.button8.Text = "福島";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.FsButtonClicked);
            // 
            // HelloLabel
            // 
            this.HelloLabel.AutoSize = true;
            this.HelloLabel.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.HelloLabel.Location = new System.Drawing.Point(100, 209);
            this.HelloLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelloLabel.Name = "HelloLabel";
            this.HelloLabel.Size = new System.Drawing.Size(142, 24);
            this.HelloLabel.TabIndex = 12;
            this.HelloLabel.Text = "???????????";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(596, 375);
            this.button9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(55, 29);
            this.button9.TabIndex = 13;
            this.button9.Text = "石川";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.IkButtonClicked);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(547, 411);
            this.button10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(61, 29);
            this.button10.TabIndex = 14;
            this.button10.Text = "富山";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.TyButtonClicked);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(867, 430);
            this.button11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(56, 29);
            this.button11.TabIndex = 15;
            this.button11.Text = "茨城";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.IgButtonClicked);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(865, 501);
            this.button12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(53, 29);
            this.button12.TabIndex = 16;
            this.button12.Text = "千葉";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.TbButtonClicked);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(803, 461);
            this.button13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(63, 29);
            this.button13.TabIndex = 17;
            this.button13.Text = "東京";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.TkButtonClicked);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(763, 394);
            this.button14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(64, 29);
            this.button14.TabIndex = 18;
            this.button14.Text = "埼玉";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.StButtonClicked);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(756, 352);
            this.button15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(51, 29);
            this.button15.TabIndex = 19;
            this.button15.Text = "栃木";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.TgButtonClicked);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(636, 425);
            this.button16.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(57, 29);
            this.button16.TabIndex = 20;
            this.button16.Text = "群馬";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.GmButtonClicked);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(779, 501);
            this.button17.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(73, 29);
            this.button17.TabIndex = 21;
            this.button17.Text = "神奈川";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.KgButtonClicked);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(596, 461);
            this.button18.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(64, 29);
            this.button18.TabIndex = 22;
            this.button18.Text = "岐阜";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.GfButtonClicked);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(685, 461);
            this.button19.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(55, 29);
            this.button19.TabIndex = 23;
            this.button19.Text = "長野";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.NnButtonClicked);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(737, 430);
            this.button20.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(53, 29);
            this.button20.TabIndex = 24;
            this.button20.Text = "山梨";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.YnButtonClicked);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(713, 501);
            this.button21.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button21.Name = "button21";
            this.button21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button21.Size = new System.Drawing.Size(57, 29);
            this.button21.TabIndex = 25;
            this.button21.Text = "静岡";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.SoButtonClicked);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(587, 501);
            this.button22.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(64, 29);
            this.button22.TabIndex = 26;
            this.button22.Text = "愛知";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.AitiButtonClicked);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(484, 418);
            this.button23.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(55, 24);
            this.button23.TabIndex = 27;
            this.button23.Text = "福井";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.FiButtonClicked);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(533, 461);
            this.button24.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(55, 29);
            this.button24.TabIndex = 28;
            this.button24.Text = "滋賀";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.SgButtonClicked);
            // 
            // HelloLabel2
            // 
            this.HelloLabel2.AutoSize = true;
            this.HelloLabel2.Font = new System.Drawing.Font("MS UI Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.HelloLabel2.Location = new System.Drawing.Point(135, 248);
            this.HelloLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HelloLabel2.Name = "HelloLabel2";
            this.HelloLabel2.Size = new System.Drawing.Size(69, 20);
            this.HelloLabel2.TabIndex = 29;
            this.HelloLabel2.Text = "??????";
            this.HelloLabel2.Click += new System.EventHandler(this.HelloLabel2_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(463, 498);
            this.button25.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(76, 29);
            this.button25.TabIndex = 30;
            this.button25.Text = "奈良";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.NrButtonClicked);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(547, 538);
            this.button26.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(76, 29);
            this.button26.TabIndex = 31;
            this.button26.Text = "三重";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.MeButtonClicked);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(468, 538);
            this.button27.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(71, 29);
            this.button27.TabIndex = 32;
            this.button27.Text = "和歌山";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.WyButtonClicked);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(447, 461);
            this.button28.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(79, 29);
            this.button28.TabIndex = 33;
            this.button28.Text = "京都";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.KtButtonClicked);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(389, 498);
            this.button29.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(65, 29);
            this.button29.TabIndex = 34;
            this.button29.Text = "大阪";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.OsButtonClicked);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(379, 430);
            this.button30.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(60, 29);
            this.button30.TabIndex = 35;
            this.button30.Text = "兵庫";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.HgButtonClicked);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(309, 430);
            this.button31.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(61, 29);
            this.button31.TabIndex = 36;
            this.button31.Text = "鳥取";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.TtButtonClicked);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(309, 482);
            this.button32.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(61, 29);
            this.button32.TabIndex = 37;
            this.button32.Text = "岡山";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.OyButtonClicked);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(235, 425);
            this.button33.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(67, 29);
            this.button33.TabIndex = 38;
            this.button33.Text = "島根";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.SnButtonClicked);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(235, 482);
            this.button34.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(67, 29);
            this.button34.TabIndex = 39;
            this.button34.Text = "広島";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.HsButtonClicked);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(157, 418);
            this.button35.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(69, 29);
            this.button35.TabIndex = 40;
            this.button35.Text = "山口";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.YguButtonClicked);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(321, 519);
            this.button36.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(60, 29);
            this.button36.TabIndex = 41;
            this.button36.Text = "香川";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.KgaButtonClicked);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(321, 555);
            this.button37.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(61, 32);
            this.button37.TabIndex = 42;
            this.button37.Text = "徳島";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.TsButtonClicked);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(235, 518);
            this.button38.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(67, 29);
            this.button38.TabIndex = 43;
            this.button38.Text = "愛媛";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.EhButtonClicked);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(235, 558);
            this.button39.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(67, 30);
            this.button39.TabIndex = 44;
            this.button39.Text = "高知";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.KtiButtonClicked);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(73, 415);
            this.button40.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(76, 29);
            this.button40.TabIndex = 45;
            this.button40.Text = "佐賀";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.SgaButtonClicked);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(105, 454);
            this.button41.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(76, 29);
            this.button41.TabIndex = 46;
            this.button41.Text = "大分";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.OiButtonClicked);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(16, 451);
            this.button42.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(76, 30);
            this.button42.TabIndex = 47;
            this.button42.Text = "福岡";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.FoButtonClicked);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(15, 489);
            this.button43.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(77, 29);
            this.button43.TabIndex = 48;
            this.button43.Text = "長崎";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.NsButtonClicked);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(105, 489);
            this.button44.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(76, 30);
            this.button44.TabIndex = 49;
            this.button44.Text = "宮崎";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.MzButtonClicked);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(16, 525);
            this.button45.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(77, 29);
            this.button45.TabIndex = 50;
            this.button45.Text = "熊本";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.KmButtonClicked);
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(105, 524);
            this.button46.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(76, 29);
            this.button46.TabIndex = 51;
            this.button46.Text = "鹿児島";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.KsButtonClicked);
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(73, 558);
            this.button47.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(76, 29);
            this.button47.TabIndex = 52;
            this.button47.Text = "沖縄";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.OnButtonClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(107, 291);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 53;
            this.label3.Text = "??????";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(955, 622);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button47);
            this.Controls.Add(this.button46);
            this.Controls.Add(this.button45);
            this.Controls.Add(this.button44);
            this.Controls.Add(this.button43);
            this.Controls.Add(this.button42);
            this.Controls.Add(this.button41);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.button39);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.HelloLabel2);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.HelloLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label HelloLabel;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Label HelloLabel2;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Label label3;
    }
}

